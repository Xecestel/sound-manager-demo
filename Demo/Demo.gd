extends Control


# Variables

@onready var bgm_result_label = get_node("Basic Features/BGMBox/ResultLabel")
@onready var bgs_result_label = get_node("Basic Features/BGSBox/ResultLabel")
@onready var se_result_label = get_node("Basic Features/SFXBox/ResultLabel")
@onready var me_result_label = get_node("Basic Features/MFXBox/ResultLabel")

############

func _on_PlayBGMFromDic_pressed():
	SoundManager.play_bgm("BGM")


func _on_PlayBGMFromPath_pressed():
	SoundManager.play_bgm("res://Assets/music.wav")


func _on_StopBGMFromDic_pressed():
	SoundManager.stop("BGM")


func _on_StopBGMFromPath_pressed():
	SoundManager.stop("res://Assets/music.wav")


func _on_PauseBGMFromDic_pressed():
	SoundManager.pause("BGM")


func _on_PauseBGMFromPath_pressed():
	SoundManager.pause("res://Assets/music.wav")


func _on_UnpauseBGMFromDic_pressed():
	SoundManager.unpause("BGM")


func _on_UnpauseBGMFromPath_pressed():
	SoundManager.unpause("res://Assets/music.wav")


func _on_SetPausedBGMFromDic_pressed():
	SoundManager.set_paused("BGM", true)


func _on_SetPausedBGMFromPath_pressed():
	SoundManager.set_paused("res://Assets/music.wav", true)


func _on_FindBGMFromDic_pressed():
	var result = SoundManager.find_sound("BGM")
	bgm_result_label.set_text(str(result))


func _on_FindBGMFromPath_pressed():
	var result = SoundManager.find_sound("res://Assets/music.wav")
	bgm_result_label.set_text(str(result))


func _on_IsPlayingBGMFromDic_pressed():
	var result = SoundManager.is_playing("BGM")
	bgm_result_label.text = str(result)


func _on_IsPlayingBGMFromPath_pressed():
	var result = SoundManager.is_playing("res://Assets/music.wav")
	bgm_result_label.text = str(result)


func _on_PlaySFXFromDic_pressed():
	SoundManager.play_sfx("SFX")


func _on_PlaySFXFromPath_pressed():
	SoundManager.play_sfx("res://Assets/chirp.wav")


func _on_StopSFXFromDic_pressed():
	SoundManager.stop("SFX")


func _on_StopSFXFromPath_pressed():
	SoundManager.stop("res://Assets/chirp.wav")


func _on_PauseSFXFromDic_pressed():
	SoundManager.pause("SFX")


func _on_PauseSFXFromPath_pressed():
	SoundManager.pause("res://Assets/chirp.wav")


func _on_UnpauseSFXFromDic_pressed():
	SoundManager.unpause("SFX")


func _on_UnpauseSFXFromPath_pressed():
	SoundManager.unpause("res://Assets/chirp.wav")


func _on_SetPausedSFXFromDic_pressed():
	SoundManager.set_paused("SFX")


func _on_SetPausedSFXFromPath_pressed():
	SoundManager.set_paused("res://Assets/chirp.wav")


func _on_FindSFXFromDic_pressed():
	var result = SoundManager.find_sound("SFX")
	se_result_label.set_text(str(result))


func _on_FindSFXFromPath_pressed():
	var result = SoundManager.find_sound("res://Assets/chirp.wav")
	se_result_label.text = str(result)


func _on_IsPlayingSFXFromDic_pressed():
	var result = SoundManager.is_playing("SFX")
	se_result_label.text = str(result)


func _on_IsPlayingSFXFromPath_pressed():
	var result = SoundManager.is_playing("res://Assets/chirp.wav")
	se_result_label.text = str(result)


func _on_PlayMFXFromDic_pressed():
	SoundManager.play_mfx("MFX")


func _on_PlayMFXFromPath_pressed():
	SoundManager.play_mfx("res://Assets/fanfare.wav")


func _on_StopMFXFromDic_pressed():
	SoundManager.stop("MFX")


func _on_StopMFXFromPath_pressed():
	SoundManager.stop("res://Assets/fanfare.wav")


func _on_PauseMFXFromDic_pressed():
	SoundManager.pause("MFX")


func _on_PauseMFXFromPath_pressed():
	SoundManager.pause("res://Assets/fanfare.wav")


func _on_UnpauseMFXFromDic_pressed():
	SoundManager.unpause("MFX")


func _on_UnpauseMFXFromPath_pressed():
	SoundManager.unpause("res://Assets/fanfare.wav")


func _on_SetPausedMFXFromDic_pressed():
	SoundManager.set_paused("MFX", true)


func _on_SetPausedMFXFromPath_pressed():
	SoundManager.set_paused("res://Assets/Assets/fanfare.wav", true)


func _on_FindMFXFromDic_pressed():
	var result = SoundManager.find_sound("MFX")
	me_result_label.text = str(result)


func _on_FindMFXFromPath_pressed():
	var result = SoundManager.find_sound("res://Assets/fanfare.wav")
	me_result_label.text = str(result)


func _on_IsPlayingMFXFromDic_pressed():
	var result = SoundManager.is_playing("MFX")
	me_result_label.text = str(result)


func _on_IsPlayingMFXFromPath_pressed():
	var result = SoundManager.is_playing("res://Assets/fanfare.wav")
	me_result_label.text = str(result)


func _on_PreloadBtn_pressed():
	SoundManager.preload_audio_files()


func _on_UnloadBtn_pressed():
	SoundManager.unload_all_resources()


func _on_PlayBGSFromDic_pressed():
	SoundManager.play_bgs("BGS")


func _on_PlayBGSFromPath_pressed():
	SoundManager.play_bgs("res://Assets/rain.ogg")


func _on_StopBGSFromDic_pressed():
	SoundManager.stop("BGS")


func _on_StopBGSFromPath_pressed():
	SoundManager.stop("res://Assets/rain.ogg")


func _on_PauseBGSFromDic_pressed():
	SoundManager.pause("BGS")


func _on_PauseBGSFromPath_pressed():
	SoundManager.pause("res://Assets/rain.ogg")


func _on_UnpauseBGSFromDic_pressed():
	SoundManager.unpause("BGS")


func _on_UnpauseBGSFromPath_pressed():
	SoundManager.unpause("res://Assets/rain.ogg")


func _on_SetPausedBGSFromDic_pressed():
	SoundManager.set_paused("BGS", true)


func _on_SetPausedBGSFromPath_pressed():
	SoundManager.set_paused("res://Assets/rain.ogg", true)


func _on_FindBGSFromDic_pressed():
	var result = SoundManager.find_sound("BGS")
	bgs_result_label.text = str(result)


func _on_FindBGSFromPath_pressed():
	var result = SoundManager.find_sound("res://Assets/rain.ogg")
	bgs_result_label.text = str(result)


func _on_IsPlayingBGSFromDic_pressed():
	var result = SoundManager.is_playing("BGS")
	bgs_result_label.text = str(result)


func _on_IsPlayingBGSFromPath_pressed():
	var result = SoundManager.is_playing("res://Assets/rain.ogg")
	bgs_result_label.text = str(result)


func _on_PreinstantiateBtn_pressed():
	SoundManager.preinstantiate_nodes()


func _on_UninstantiateBtn_pressed():
	SoundManager.uninstantiate_all_nodes(true)


func _on_PreloadSoundFromDic_pressed():
	SoundManager.preload_resource_from_string("BGM")


func _on_PreloadSoundFromPath_pressed():
	SoundManager.preload_resource_from_string("res://Assets/music.wav")


func _on_PreinstantiateNodeFromDic_pressed():
	SoundManager.preinstantiate_node_from_string("BGM")


func _on_PreinstantiaeNodeFromPath_pressed():
	SoundManager.preinstantiate_node_from_string("res://Assets/music.wav")


func _on_UnloadSoundFromDic_pressed():
	SoundManager.unload_resource_from_string("BGM")


func _on_UnloadSoundFromPath_pressed():
	SoundManager.unload_resource_from_string("res://Assets/music.wav")


func _on_UninstantiateSoundFromDic_pressed():
	SoundManager.uninstantiate_node_from_string("BGM")


func _on_UninstantiateSoundFromPath_pressed():
	SoundManager.uninstantiate_node_from_string("res://Assets/music.wav")


func _on_fade_out_bgm_from_dic_pressed():
	SoundManager.fade_out("BGM", 5.0)


func _on_fade_out_bgm_from_path_pressed():
	SoundManager.fade_out("res://Assets/music.wav", 5.0)


func _on_fade_inbgm_from_dic_pressed():
	SoundManager.fade_in_bgm("BGM", 5.0)


func _on_fade_in_bgs_from_dic_pressed():
	SoundManager.fade_in_bgs("BGS", 5.0)


func _on_fade_out_bgs_from_dic_pressed():
	SoundManager.fade_out("BGS", 5.0)


func _on_fade_in_bgs_from_path_pressed():
	SoundManager.fade_in_bgs("res://Assets/rain.ogg", 5.0)


func _on_fade_out_bgs_from_path_pressed():
	SoundManager.fade_out("res://Assets/rain.ogg", 5.0)


func _on_fade_in_se_from_dic_pressed():
	SoundManager.fade_in_sfx("SFX", 1.0)


func _on_fade_in_se_from_path_pressed():
	SoundManager.fade_in_sfx("res://Assets/chirp.wav", 1.0)


func _on_fade_out_se_from_dic_pressed():
	SoundManager.fade_out("SFX", 1.0)


func _on_fade_out_se_from_path_pressed():
	SoundManager.fade_out("res://Assets/chirp.wav", 1.0)


func _on_fade_in_mfx_from_dic_pressed():
	SoundManager.fade_in_mfx("MFX", 1.0)


func _on_fade_in_mfx_from_path_pressed():
	SoundManager.fade_in_mfx("res://Assets/fanfare.wav", 1.0)


func _on_fade_out_mfx_from_dic_pressed():
	SoundManager.fade_out("MFX", 1.0)


func _on_fade_out_mfx_from_path_pressed():
	SoundManager.fade_out("res://Assets/fanfare.wav", 1.0)


func _on_fade_into_bgm_from_dic_pressed() -> void:
	SoundManager.fade_into_bgm("BGM", "BGM", 5.0)


func _on_fade_into_bgm_from_path_pressed() -> void:
	SoundManager.fade_into_bgm("res://Assets/music.wav", "BGM", 5.0)


func _on_fade_into_bgs_from_dic_pressed() -> void:
	SoundManager.fade_into_bgs("BGS", "BGS", 5.0)


func _on_fade_into_bgs_from_path_pressed() -> void:
	SoundManager.fade_into_bgs("res://Assets/rain.ogg", "BGS", 5.0)


func _on_fade_into_sfx_from_path_pressed() -> void:
	SoundManager.fade_into_sfx("res://Assets/chirp.wav", "SFX", 1.0)


func _on_fade_into_sfx_from_dic_pressed() -> void:
	SoundManager.fade_into_sfx("SFX", "SFX", 1.0)


func _on_fade_into_mfx_from_dic_pressed() -> void:
	SoundManager.fade_into_mfx("MFX", "MFX", 1.0)


func _on_fade_into_mfx_from_path_pressed() -> void:
	SoundManager.fade_into_mfx("res://Assets/fanfare.wav", "MFX", 1.0)


func _on_stop_all_btn_pressed():
	SoundManager.stop_all()


func _on_pause_all_btn_pressed():
	SoundManager.pause_all()


func _on_unpause_all_btn_pressed():
	SoundManager.unpause_all()


func _on_north_btn_pressed():
	SoundManager.play_sfx_from("SFX", "Front")


func _on_east_btn_pressed():
	SoundManager.play_sfx_from("SFX", "East")


func _on_west_btn_pressed():
	SoundManager.play_sfx_from("SFX", "West")
