# Sound Manager Plugin Demo
## Version 2.3
## © Xecestel 2021-2024
## Licensed Under MPL 2.0 (see below)

## Overview

This is the official demo for the [Sound Manager Plugin](https://gitlab.com/Xecestel/sound-manager).  
You can use it to test the plugin, understand how it works and mess around with it as you like. It features almost all the things you can do with the Sound Manager.  
    
You can download the plugin from:
- [GitLab](https://gitlab.com/Xecestel/sound-manager)
- [itch.io](https://xecestel.itch.io/sound-manager-plugin)
- [Godot AssetLib](https://godotengine.org/asset-library/asset/361)

## How to install

Download the repository and put it whenever you like on your computer. Then just open Godot, import the project.godot file on the engine and click "Edit".  


## IMPORTANT NOTES:
For more information about how the Sound Manager Module works, read the [README](./addons/sound_manager/README.md) file inside the plugin directory.  


## Credits
This demo was developed by Celeste Privitera (@Xecestel) as an official demo for the Sound Manager Plugin.  
  
The Sound Manager Plugin was developed by Simón Olivo (@Sarturo) and Celeste Privitera (@Xecestel) as a derivative work from the [Sound Manager Module](./addons/sound_manager/module) originally developed by Celeste Privitera.  

## Licenses
### Sound Manager Demo
Copyright © 2020-2024 Celeste Priviera  
  
The Sound Manager Demo is subject to the terms of the MIT License.  
If a copy of the license was not distributed with this file, You can obtain one at [https://mit-license.org/](https://mit-license.org/).  

### Sound Manager Module
Copyright © 2019-2024 Celeste Privitera  
  
The Sound Manager Module is subject to the terms of the MIT License.  
If a copy of the license was not distributed with this file, You can obtain one at [https://mit-license.org/](https://mit-license.org/).  

### Sound Manager Plugin
Copyright © 2019-2024 Simón Olivo & Celeste Privitera  

The Sound Manager Plugin is subject to the terms of the Mozilla Public License, v. 2.0.  
If a copy of the MPL was not distributed with this file, You can obtain one at [https://mozilla.org/MPL/2.0/](https://mozilla.org/MPL/2.0/).  

All the audio files contained in this project are subject to the terms of the [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/deed.it)

## Changelog
### Version 1.0
- Initial commit

### Version 1.1
- Enabled the plugin that got disabled by mistake before releasing
- Fixed an error some of the buttons' text
- Added new buttons to test preloading and preinstantiation of single sounds

### Version 2.0
- Support for Sound Manager Plugin 4
- Updated to Godot Engine 4
- Updated GUI to account for monitors of different sizes

### Version 2.1
- Updated to Sound Manager Plugin 4.1
- Updated GUI to reflect the changes

### Version 2.2
- Updated to Sound Manager Plugin 4.3

### Version 2.3
- Updated to Sound Manager Plugin 4.4
- Updated GUI to reflect the changes
